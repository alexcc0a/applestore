import ru.store.IPhone;
import ru.store.MacBook;
import ru.store.MacStudio;
import ru.store.Product;

import java.util.Scanner;

import static ru.store.Constants.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Добро пожаловать в магазин Apple!");
        // Создается массив products типа Product и инициализируется объектами IPhone, MacBook и MacStudio.
        Product[] products = {
            new IPhone(),
            new MacBook(),
            new MacStudio(),
        };

        // Вызывается метод printProducts с аргументом products.
        printProducts(products);
        // Вызывается метод separate.
        separate();

        Scanner scanner = new Scanner(System.in);
        // Бесконечный цикл.
        while(true) {
            // Выводится сообщение "Выберете товар и количество, или введите end".
            System.out.println("Выберете товар и количество, или введите end");
            // Вызывается метод separate.
            separate();
            // Считывается строка из ввода пользователя.
            String line = scanner.nextLine();
            // Если строка равна "end", то цикл прерывается.
            if(line.equals("end")) {
                break;
            }
            // В блоке try выполняется парсинг строки и обработка исключений.
            try {
                // Строка разбивается на подстроки по пробелу.
                String[] split = line.split(" ");
                // Если количество подстрок больше 2, выбрасывается исключение ArrayIndexOutOfBoundsException.
                if (split.length > 2) {
                    throw new ArrayIndexOutOfBoundsException();
                } else {
                    // Иначе, первая подстрока парсится в число и вычитается 1.
                    int numberProduct = Integer.parseInt(split[0]) - 1;
                    // К количеству покупок продукта прибавляется вторая подстрока, парсенная в число.
                    int countBuy = products[numberProduct].getCountBuy() + Integer.parseInt(split[1]);
                    // Устанавливается новое значение количества покупок продукта.
                    products[numberProduct].setCountBuy(countBuy);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                // Если возникает исключение ArrayIndexOutOfBoundsException, выводится сообщение об ошибке.
                System.out.println("Сначала написать номер товара который вы хотите приобрести, " +
                        "потом через пробел ввести колличество.");
                // Вызывается метод separate.
                separate();
            }

        }
        // Вызывается метод separate.
        separate();
        System.out.println("Корзина:");

        // Вызывается метод printShoppingCart с аргументом products.
        printShoppingCart(products);
    }

    // Объявление публичного статического метода printProducts с аргументом products типа Product[].
    public static void printProducts(Product[] products) {
        // Цикл for перебирает элементы массива products.
        for (int i = 0; i < products.length; i++) {
            // Выводится строка, содержащая порядковый номер элемента, название продукта, цену и валюту.
            System.out.println((i+1) + ". " + products[i] + " - " + products[i].getPrice() + CURRENCY);
        }
    }

    // Объявление публичного статического метода separate.
    public static void separate() {
        // Выводится разделительная строка.
        System.out.println("---------------------------------------------------------------------------------------");
    }

    // Объявление публичного статического метода printShoppingCart с аргументом products типа Product[].
    public static void printShoppingCart(Product[] products) {
        // Объявление переменной totalAmountProduct типа double и инициализация значением 0.
        double totalAmountProduct = 0;
        // Цикл for each перебирает элементы массива products.
        for (Product product : products) {
            // Если количество покупок продукта больше 0, выполняются следующие действия.
            if (product.getCountBuy() > 0) {
                // Вычисляется сумма покупки продукта.
                double amountProduct = product.getPrice() * product.getCountBuy();
                // Сумма покупки добавляется к общей сумме покупок.
                totalAmountProduct += amountProduct;
                // Выводится строка, содержащая название продукта, количество покупок и сумму покупки.
                System.out.println(product + " (" + product.getCountBuy() + " шт.) " + " " + amountProduct + CURRENCY);
            }
        }
        // Выводится строка, содержащая общую сумму покупок.
        System.out.println("К оплате: " + totalAmountProduct + CURRENCY);
    }
}