package ru.store;

public class MacBook extends Product implements WorkPrinciple{

    private final String name = "Apple MacBook Pro 14 (M2 Pro 12C CPU, 19C GPU, 2023).";
    private final double price = 289_990;

    // Переопределение метода toString.
    @Override
    public String toString() {
        return name;
    }

    // Переопределение метода getPrice.
    @Override
    public double getPrice() {
        return price;
    }

    // Реализация метода workPrinciple интерфейса WorkPrinciple.
    @Override
    public void workPrinciple() {
        System.out.println("MacBook включен");
    }
}
