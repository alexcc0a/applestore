package ru.store;

public class IPhone extends Product implements WorkPrinciple, Storage{

    // Объявление приватной константы name типа String со значением "iPhone 15 Pro.".
    private final String name = "iPhone 15 Pro.";
    private final double price = 149_990;

    // Переопределение метода toString.
    @Override
    public String toString() {
        return name;
    }

    // Переопределение метода getPrice.
    @Override
    public double getPrice() {
        return price;
    }

    // Реализация метода workPrinciple интерфейса WorkPrinciple.
    @Override
    public void workPrinciple() {
        System.out.println("iPhone 15 Pro куплен");
    }

    // Реализация метода storage интерфейса Storage.
    @Override
    public void storage() {
        System.out.println("iPhone 15 Pro взят с собой");
    }
}
