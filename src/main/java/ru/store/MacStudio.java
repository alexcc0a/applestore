package ru.store;

public class MacStudio extends Product implements WorkPrinciple, Storage{

    private final String name = "Apple Mac Studio (M2 Ultra, 2023).";
    private final double price = 599_990;

    // Переопределение метода toString.
    @Override
    public String toString() {
        return name;
    }

    // Переопределение метода getPrice.
    @Override
    public double getPrice() {
        return price;
    }

    // Реализация метода workPrinciple интерфейса WorkPrinciple.
    @Override
    public void workPrinciple() {
        System.out.println("Mac Studio куплен!");
    }

    // Реализация метода storage интерфейса Storage.
    @Override
    public void storage() {
        System.out.println("Mac Studio взят с собой!");
    }
}
